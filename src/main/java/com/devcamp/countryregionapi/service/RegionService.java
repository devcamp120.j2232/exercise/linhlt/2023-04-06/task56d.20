package com.devcamp.countryregionapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.countryregionapi.model.Region;
@Service
public class RegionService {
    Region region1 = new Region("HAN","Hanoi");
    Region region2 = new Region("HCM","Hochiminh");
    Region region3 = new Region("CALI","California");
    Region region4 = new Region("WAH","Washington");
    Region region5 = new Region("MEL","Melbourn");
    Region region6 = new Region("SYD","Sydney");
    Region region7 = new Region("BAK","Bangkok");
    Region region8 = new Region("PHU","Phuket");
    Region region9 = new Region("SHA","Shanghai");
    Region region10 = new Region("BEJ","Beijing");
    Region region11 = new Region("TOR","Toronto");
    Region region12 = new Region("VAN","Vancouver");
    public ArrayList<Region> listRegionsVN(){
        ArrayList<Region> regionsVN = new ArrayList<>();
        regionsVN.add(region1);
        regionsVN.add(region2);
        return regionsVN;
    }
    public ArrayList<Region> listRegionsUSA(){
        ArrayList<Region> regionsUSA = new ArrayList<>();
        regionsUSA.add(region3);
        regionsUSA.add(region4);
        return regionsUSA;
    }
    public ArrayList<Region> listRegionsAUS(){
        ArrayList<Region> regionsAUS = new ArrayList<>();
        regionsAUS.add(region5);
        regionsAUS.add(region6);
        return regionsAUS;
    }
    public ArrayList<Region> listRegionsTHA(){
        ArrayList<Region> regionsTHA = new ArrayList<>();
        regionsTHA.add(region7);
        regionsTHA.add(region8);
        return regionsTHA;
    }
    public ArrayList<Region> listRegionsCHN(){
        ArrayList<Region> regionsCHN = new ArrayList<>();
        regionsCHN.add(region9);
        regionsCHN.add(region10);
        return regionsCHN;
    }
    public ArrayList<Region> listRegionsCAN(){
        ArrayList<Region> regionsCAN = new ArrayList<>();
        regionsCAN.add(region11);
        regionsCAN.add(region12);
        return regionsCAN;
    }
    public ArrayList<Region> allRegionList(){
        ArrayList<Region> regionList = new ArrayList<>();
        regionList.add(region1);
        regionList.add(region2);
        regionList.add(region3);
        regionList.add(region4);
        regionList.add(region5);
        regionList.add(region6);
        regionList.add(region7);
        regionList.add(region8);
        regionList.add(region9);
        regionList.add(region10);
        regionList.add(region11);
        regionList.add(region12);
        return regionList;
    }
}
